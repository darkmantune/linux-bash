#!/bin/bash
#

# variables color
negrita=`echo -en "\e[1m"`
rojo=`echo -en "\e[31m"`
rojoLuz=`echo -en "\e[91m"`

verde=`echo -en "\e[32m"`
verdeLuz=`echo -en "\e[92m"`

azul=`echo -en "\e[34m"`
azulLuz=`echo -en "\e[94m"`

aqua=`echo -en "\e[36m"`
aquaLuz=`echo -en "\e[96m"`

amarillo=`echo -en "\e[93m"`

normal=`echo -en "\e[0m"`

comenta=`echo -en "\e[96m"`
copy=`echo -en "\e[92m"`

## presentacion
clear
echo -n "$negrita" #-n es para que no haga retorno de carro
echo "---------------------------------------------------"
echo "         XRDP - Cambiar Idioma Español"
echo '---------------------------------------------------'
echo
echo '   XRDP cambiar teclado a español'
echo 
echo '---------------------------------------------------'
echo " ${copy}by fitopaisa@hotmail.com - www.darkmantune.com${normal}"
echo
echo 'Basado en el tutorial de:'
echo 'http://scriptbash.blogspot.com.es/2012/04/xrdp-cambiar-teclado-espanol.html'

echo;echo

echo '---------------------------------------------------'
echo 'Descargar teclado en español'
echo 'http://www.mail-archive.com/xrdp-devel%40lists.sourceforge.net/msg00192/km-040a.ini.gz'
echo '---------------------------------------------------'


###############
echo "${normal}${negrita}${verdeLuz}"
echo 'XRDP - Cambiando Idioma Español'
echo -n "$comenta"
echo "Ejecuta setxkbmap con el idioma español"
echo "y teclado estandar de 105 teclas."
echo "${normal}${negrita}"
read -p "sudo setxkbmap -layout 'es,es' -model pc105 "
echo -n "${normal}"
sudo setxkbmap -layout 'es,es' -model pc105
echo 
echo "$comenta"
echo "carga el nuevo teclado con el con:"
echo "${normal}${negrita}"
read -p "xrdp-genkeymap /etc/xrdp/km-040a.ini "
echo -n "${normal}"
xrdp-genkeymap /etc/xrdp/km-040a.ini

echo "$comenta"
echo 'Si no te salio ningun error perfecto'
echo -n "${normal}"
echo 'dale a control+c para salir'
echo
echo 'Si esto no te funcionó continua con el script'

echo "${normal}${negrita}${verdeLuz}"
echo 'Cambiando el mapeo del teclado ingles al español'

echo "$normal"
echo 'hacemos una copa de respaldo del archivo de teclado original'
echo 'km-0409.ini que contiene el mapeo de las teclas en ingles.'
echo 'y lo reemplazamos por la version en español'
echo

echo "${comenta}"
echo 'copia de respaldo'
echo -n "${normal}${negrita}"
read -p "cp /etc/xrdp/km-0409.ini /etc/xrdp/km-0409.ini.old "
echo -n "${normal}"
sudo cp /etc/xrdp/km-0409.ini /etc/xrdp/km-0409.ini.old

echo "${comenta}"
echo 'Reemplazando el archivo modificado al español'
echo -n "${normal}${negrita}"
read -p "cp km-0409.ini /etc/xrdp/km-0409.ini"
echo -n "${normal}"
sudo cp km-0409.ini /etc/xrdp/km-0409.ini

echo -n "${normal}"


read -p 'Instalado correctamente: '