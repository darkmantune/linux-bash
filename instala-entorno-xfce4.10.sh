#!/bin/bash

# variables color
source estilos/estilos.sh

color

function presentacion {  
clear
echo -n "$negrita" #-n es para que no haga retorno de carro
echo "---------------------------------------------------"
echo "         Instalando programas"
echo '---------------------------------------------------'
echo
echo '   Instalando entorno XFCE 4.0.'
echo 
echo '---------------------------------------------------'
echo " ${copy}by fitopaisa@hotmail.com - www.darkmantune.com${normal}"
echo;echo
}

presentacion



###############
# instalacion XFCE 4.0
#
echo "${normal}${negrita}${verdeLuz}"
echo 'Instalando XFCE 4.0.'
echo -n "$comenta"
echo "Entorno grafico mas ligero"
echo "${normal}${negrita}"
read -p "sudo add-apt-repository ppa:xubuntu-dev/xfce-4.10 "
echo -n "${normal}"
sudo add-apt-repository ppa:xubuntu-dev/xfce-4.10
echo 
read -p 'Instalado repositorio correctamente: '
echo
echo 'hacemos un apdate antes de instalar el entorno'
echo
read -p "sudo apt-get update "
echo -n "${normal}"
sudo apt-get update
echo 
echo 'actualizamos repositorio'
read -p 'sudo apt-get upgrade: '
sudo apt-get upgrade
echo 

echo 'ahora si podemos instalarlo' 
echo 'Instalamos XFCE4'
read -p 'sudo apt-get install xfce4 '
sudo apt-get install xfce4
echo 
echo
echo 'Instalado XFCE4'
echo 'Cierra sesion o reinicia para usar el nuevo escritorio'


footer